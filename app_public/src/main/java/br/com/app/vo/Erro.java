package br.com.app.vo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Erro implements Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Erro() {
    }


    private Integer codigo;
    private String descricao;

    public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
    public String toString() {
	return "Erro [codigo=" + codigo + ", descricao=" + descricao + "]";
    }

}
