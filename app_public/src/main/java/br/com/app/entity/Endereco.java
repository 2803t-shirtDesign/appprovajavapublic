package br.com.app.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlRootElement;


@Entity
@XmlRootElement  
public class Endereco {
	
	@Id 
    @GeneratedValue(strategy=GenerationType.AUTO) 
	public Integer idEndereco;

	@Column(name="cep", nullable=false, unique=true)
	private String cep;
	
	@Column(name="rua", nullable=false)
	private String rua;
	
	@Column(name="bairro", nullable=true)
	private String bairro;
	
	@Column(name="cidade", nullable=false)
	private String cidade;
	
	@Column(name="estado", nullable=false)
	private String estado;
	
	@Column(name="numero", nullable=false)
	private String numero;
	
	@Column(name="complemento", nullable=true)
	private String complemento;
	
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public Integer getIdEndereco() {
		return idEndereco;
	}

	public void setIdEndereco(Integer idEndereco) {
		this.idEndereco = idEndereco;
	}
	
	@Override
	 public String toString() {
	 return "Endereco [cep=" + cep + ", rua=" + rua	 + ", bairro=" + bairro + ", cidade=" + cidade + ", estado=" + estado + ", numero=" + numero + ", complemento=" + complemento + "]";
	 }

}
