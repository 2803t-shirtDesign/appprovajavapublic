package br.com.app.logica.vogal;

public interface Stream {

	public char getNext();

	public boolean hasNext();

}
