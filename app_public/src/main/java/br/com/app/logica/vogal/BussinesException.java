package br.com.app.logica.vogal;

/**
 * Exceção lançada quando não for identificado caracter repetido.
 * 
 */
@SuppressWarnings("serial")
public class BussinesException extends Exception {

	/**
	 * Construtor padrão.
	 */
	public BussinesException() {
		super("Não foram encontrados caracteres repetidos.");
	}

}
