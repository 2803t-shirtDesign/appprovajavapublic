package br.com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = {"br.com.app.entity"})
@EnableJpaRepositories(basePackages = {"br.com.app.repository"})
@ComponentScan(basePackages = {"br.com.app.controllers, br.com.app.service"})
public class AppApplication {
      public static void main(String[] args) {
            SpringApplication.run(AppApplication.class, args);
      }
}

