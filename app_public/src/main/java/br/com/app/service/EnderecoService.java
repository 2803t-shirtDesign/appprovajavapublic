package br.com.app.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.h2.jdbc.JdbcSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import br.com.app.entity.Endereco;
import br.com.app.pojo.PojoEnderecoCorreiosJson;
import br.com.app.repository.EnderecoRepository;
import br.com.app.vo.EnderecoVO;

@Service
public class EnderecoService {
	
	private static String uri_correios = "http://viacep.com.br/ws/";
	private static String type_return_service = "/json/"; 
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	/**
	 * Metodo Responsavel por realizar o cast do objeto entity para o VO
	 * @param endereco
	 * @return
	 */
	public EnderecoVO enderecoToVO(Endereco endereco){
		EnderecoVO  enderecoVO = null;
		if (endereco != null) {
			enderecoVO = new EnderecoVO();
			enderecoVO.setIdEndereco(endereco.getIdEndereco());
			enderecoVO.setBairro(endereco.getBairro());
			enderecoVO.setCep(endereco.getCep());
			enderecoVO.setCidade(endereco.getCidade());
			enderecoVO.setEstado(endereco.getEstado());
			enderecoVO.setRua(endereco.getRua());
			enderecoVO.setComplemento(endereco.getComplemento());
			enderecoVO.setNumero(endereco.getNumero());
		}
		return enderecoVO;
	}
	
	/**
	 * Metodo Responsavel por realizar o cast do objeto VO para o Entity
	 * @param endereco
	 * @return
	 */
	public Endereco enderecoToEntity(EnderecoVO enderecoVO){
		Endereco endereco = null;
		if (enderecoVO != null) {
			endereco = new Endereco();
			endereco.setIdEndereco(enderecoVO.getIdEndereco());
			endereco.setBairro(enderecoVO.getBairro());
			endereco.setCep(enderecoVO.getCep());
			endereco.setCidade(enderecoVO.getCidade());
			endereco.setEstado(enderecoVO.getEstado());
			endereco.setRua(enderecoVO.getRua());
			endereco.setComplemento(enderecoVO.getComplemento());
			endereco.setNumero(enderecoVO.getNumero());
		}
		return endereco;
	}
	
	/**
	 * Metodo Responsavel por realizar o cast do objeto entity para o VO
	 * @param endereco
	 * @return
	 */
	public EnderecoVO pojoEnderecoCorreiosToVO(PojoEnderecoCorreiosJson endereco){
		EnderecoVO enderecoVO = new EnderecoVO();
		enderecoVO.setBairro(endereco.getBairro());
		enderecoVO.setCep(endereco.getCep());
		enderecoVO.setCidade(endereco.getLocalidade());
		enderecoVO.setEstado(endereco.getUf());
		enderecoVO.setRua(endereco.getLogradouro());
		return enderecoVO;
	}
	
	/**
	 * Lista todos os enderecos cadastrados
	 * @return List<EnderecoVO>
	 */
	public List<EnderecoVO> listAllEnderecos(){
		List<EnderecoVO> listEnderecoVO = new ArrayList<EnderecoVO>();
		List<Endereco> listEndereco = enderecoRepository.findAll();
		for (Endereco endereco : listEndereco) {
			EnderecoVO enderecoVO = new EnderecoVO();
			enderecoVO = enderecoToVO(endereco);
			listEnderecoVO.add(enderecoVO);
		}
		return listEnderecoVO;
	}
	
	/**
	 * Metodo reponsavel por realizar a logica de busca de CEP na base de dados e com ZERO a direita
	 * @param cep
	 * @return enderecoVO
	 */
	public EnderecoVO findEnderecoByCep(EnderecoVO enderecoVO){
		//Busca Cep base dados
		Endereco endereco = enderecoRepository.findByCep(enderecoVO.getCep());
		if (endereco != null) {
			enderecoVO = enderecoToVO(endereco);
			return enderecoVO;
		}else{
			//Logica para adicionar valor Zero a direita da String
			for (int i = 1; i < enderecoVO.getCep().length(); i++) {
				StringBuilder pattern = new StringBuilder();
				pattern.append(enderecoVO.getCep().substring(0, enderecoVO.getCep().length()-i));
				for (int j = 0; j < i; j++) {
					pattern = pattern.append("0");
				}
				//Busca Cep base de Dados
				System.out.println("pattern base: " + pattern.toString());
				endereco = enderecoRepository.findByCep(pattern.toString());
				if (endereco != null) {
					enderecoVO = enderecoToVO(endereco);
					return enderecoVO;
				}
			}
			return 	verificaCepCorrecios(enderecoVO.getCep(), enderecoVO);
		}
	}
	
	/**
	 * Metodo reponsavel por realizar a logica de busca de CEP no correios e com ZERO a direita
	 * @param cep
	 * @return enderecoVO
	 */
	public EnderecoVO verificaCepCorrecios(String cep, EnderecoVO enderecoVO){
		EnderecoVO enderecoAux = findEnderecoCorreiosByCep(enderecoVO.getCep()); 
		//Busca Cep Correios
		if (enderecoAux != null) {
			enderecoVO.setBairro(enderecoAux.getBairro());
			enderecoVO.setCep(enderecoAux.getCep());
			enderecoVO.setCidade(enderecoAux.getCidade());
			enderecoVO.setEstado(enderecoAux.getEstado());
			enderecoVO.setRua(enderecoAux.getRua());
			return enderecoVO;
		}else{
			//Logica para adicionar valor Zero a direita da String
			for (int i = 1; i < cep.length(); i++) {
				StringBuilder pattern = new StringBuilder();
				pattern.append(cep.substring(0, cep.length()-i));
				for (int j = 0; j < i; j++) {
					pattern = pattern.append("0");
				}
				//Busca Cep Correios
				enderecoAux = findEnderecoCorreiosByCep(pattern.toString());
				if (enderecoAux != null) {
					enderecoVO.setBairro(enderecoAux.getBairro());
					enderecoVO.setCep(enderecoAux.getCep());
					enderecoVO.setCidade(enderecoAux.getCidade());
					enderecoVO.setEstado(enderecoAux.getEstado());
					enderecoVO.setRua(enderecoAux.getRua());
					return enderecoVO;
				}
			}
		}
		return enderecoVO;
	}
	
	/**
	 * Metodo resposavel por buscar endereco nos correios
	 * @param cep
	 * @return enderecoVO
	 */
	public EnderecoVO findEnderecoCorreiosByCep(String cep){
		EnderecoVO enderecoVO = null;
		//Cria a chamada para servicos do correio
	    Client client = ClientBuilder.newClient();
	    System.out.println("Busca Correios: " + cep);
        Response response = client.target(uri_correios).path(cep).path(type_return_service)
                                  .request(MediaType.APPLICATION_JSON).get();
        //Verifica status HTTP Sucesso
        if (response.getStatus() == HttpStatus.OK.value()) {
            try {
            	PojoEnderecoCorreiosJson entity = response.readEntity(PojoEnderecoCorreiosJson.class);
            	System.out.println(entity);
                enderecoVO = pojoEnderecoCorreiosToVO(entity);
                return enderecoVO;
    		} catch (Throwable e) {
    			//Json com ERRO, nao foi possicel realizar o parse para Pojo
    			return enderecoVO;
    		}
		}else{
			return enderecoVO;
		}
	}
	
	/**
	 * Metodo responsavel por salvar o Endereco
	 * @param enderecoVO
	 * @return enderecoVO
	 */
	public EnderecoVO add_alter(EnderecoVO enderecoVO){
		try {
			enderecoRepository.saveAndFlush(enderecoToEntity(enderecoVO));
			return enderecoVO;	
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Metodo responsavel por deletar o Endereco
	 * @param enderecoVO
	 * @return enderecoVO
	 */
	public void delete(Integer id){
		try {
			enderecoRepository.delete(enderecoRepository.findByIdEndereco(id));
		} catch (Exception e) {
			//Lanca Exception caso houver delete
			throw e;
		}
	}
	
	/**
	 * Busca Endereco por id
	 * @param id
	 * @return enderecoVO
	 */
	public EnderecoVO findById(Integer id){
		return enderecoToVO(enderecoRepository.findByIdEndereco(id));
	}
}
