package br.com.app.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.app.service.EnderecoService;
import br.com.app.vo.EnderecoVO;
import br.com.app.vo.Erro;

@RestController
@RequestMapping("/")
public class EnderecoController {
	
	@Autowired
	private EnderecoService enderecoService;

	public EnderecoController() {
	}
	
	/**
	 * GET - Metodo Responsavel por listar todos os enderecos cadastrados na base de dados
	 * @return ResponseEntity<List<EnderecoVO>
	 */
	@RequestMapping(value = "/enderecos", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	public ResponseEntity<List<EnderecoVO>> listar() {
		List<EnderecoVO> enderecos =  enderecoService.listAllEnderecos();
		if (enderecos.size() != 0) {
			return new ResponseEntity<List<EnderecoVO>>(new ArrayList<EnderecoVO>(enderecos), HttpStatus.OK);
		}else{
			EnderecoVO enderecoVO = new EnderecoVO();
			Erro erro = new Erro();
			erro.setDescricao("Nenhum endereco cadastrado");
			erro.setCodigo(HttpStatus.BAD_REQUEST.value());
			enderecoVO.setErro(erro);
			enderecos.add(enderecoVO);
			return new ResponseEntity<List<EnderecoVO>>(new ArrayList<EnderecoVO>(enderecos), HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Metodo responsavel por consultar um endereco por CEP
	 * @param cep
	 * @return ResponseEntity<EnderecoVO>
	 */
	@RequestMapping(value = "/endereco/{cep}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<EnderecoVO> findByCep(@PathVariable String cep) {
		cep = cep. replaceAll("[-+.^:,@]","");
		EnderecoVO enderecoVO = new EnderecoVO();
		enderecoVO.setCep(cep);
		enderecoVO = enderecoService.findEnderecoByCep(enderecoVO);
		if (enderecoVO != null) {
			return new ResponseEntity<EnderecoVO>((EnderecoVO) enderecoVO, HttpStatus.OK);	
		}else{
			enderecoVO = new EnderecoVO();
			Erro erro = new Erro();
			erro.setDescricao("Cep Nao Encontrado");
			erro.setCodigo(HttpStatus.BAD_REQUEST.value());
			enderecoVO.setErro(erro);
			return new ResponseEntity<EnderecoVO>((EnderecoVO) enderecoVO, HttpStatus.BAD_REQUEST);	
		}
		
	}

	@RequestMapping(value = "/add-endereco", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public ResponseEntity<EnderecoVO> add(@RequestBody EnderecoVO endereco) {
		if (endereco != null && endereco.getCep() != null) {
			//Remove caracteres especiais
			endereco.setCep(endereco.getCep().replaceAll("[-+.^:,@]",""));
			if (endereco.getCep().length() != 8) {
				endereco = new EnderecoVO();
				Erro erro = new Erro();
				erro.setDescricao("Cep Inválido");
				erro.setCodigo(HttpStatus.BAD_REQUEST.value());
				endereco.setErro(erro);
				return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.BAD_REQUEST);	
			}else{
				if (enderecoService.findEnderecoByCep(endereco) != null) {
					try {
						//Salva endereco na base de dados
						endereco.setCep(endereco.getCep().replaceAll("[-+.^:,@]",""));
						enderecoService.add_alter(endereco);
						return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.OK);	
					} catch (Exception e) {
						Erro erro = new Erro();
						erro.setDescricao("Nao foi possivel adicionar endereco, verifique os dados");
						erro.setCodigo(HttpStatus.BAD_REQUEST.value());
						endereco.setErro(erro);
						return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.BAD_REQUEST);
					}
				}else{
					endereco = new EnderecoVO();
					Erro erro = new Erro();
					erro.setDescricao("Cep Nao Encontrado");
					erro.setCodigo(HttpStatus.BAD_REQUEST.value());
					endereco.setErro(erro);
					return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.BAD_REQUEST);	
				}
			}
		} else {
			endereco = new EnderecoVO();
			Erro erro = new Erro();
			erro.setDescricao("Dados Inválidos");
			erro.setCodigo(HttpStatus.BAD_REQUEST.value());
			endereco.setErro(erro);
			return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.BAD_REQUEST);	
		}
	}
	
	/**
	 * Metodo responsavel por alterar os dadso do endereco
	 * @param endereco
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/alter-endereco/{id}", method = RequestMethod.PUT, consumes = "application/json", produces = "application/json")
	public ResponseEntity<EnderecoVO> alter(@RequestBody EnderecoVO endereco, @PathVariable Integer id) {
		
		if (id != null && endereco.getCep() != null) {
			//Remove caracteres especiais
			endereco.setCep(endereco.getCep().replaceAll("[-+.^:,@]",""));
			if (endereco != null && endereco.getCep().length() != 8) {
				endereco = new EnderecoVO();
				Erro erro = new Erro();
				erro.setDescricao("Dados Inválidos");
				erro.setCodigo(HttpStatus.BAD_REQUEST.value());
				endereco.setErro(erro);
				return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.BAD_REQUEST);	
			}else{
				if (enderecoService.findById(id) != null) {
					try {
						//Altera Endereco
						endereco.setCep(endereco.getCep().replaceAll("[-+.^:,@]",""));
						endereco.setIdEndereco(id);
						endereco = enderecoService.add_alter(endereco);
						return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.OK);
					} catch (Exception e) {
						Erro erro = new Erro();
						erro.setDescricao("Nao foi possivel realizar alteracao, verifique os dados");
						erro.setCodigo(HttpStatus.BAD_REQUEST.value());
						endereco.setErro(erro);
						return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.BAD_REQUEST);
					}
				}else{
					endereco = new EnderecoVO();
					Erro erro = new Erro();
					erro.setDescricao("Endereco Nao Encontrado");
					erro.setCodigo(HttpStatus.BAD_REQUEST.value());
					endereco.setErro(erro);
					return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.BAD_REQUEST);	
				}
			}
		} else {
			endereco = new EnderecoVO();
			Erro erro = new Erro();
			erro.setDescricao("Dados Inválidos");
			erro.setCodigo(HttpStatus.BAD_REQUEST.value());
			endereco.setErro(erro);
			return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.BAD_REQUEST);	
		}
		
	}
	
	/**
	 * Metoque responsavel por excluir o registro de endereco
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/delete-endereco/{id}", method = RequestMethod.DELETE, consumes = "application/json", produces = "application/json")
	public ResponseEntity<EnderecoVO> delete(@PathVariable Integer id) {

		if (id != null) {
			EnderecoVO endereco = enderecoService.findById(id);
			if (endereco != null) {
				try {
					//Endereco Endereco
					enderecoService.delete(id);
					endereco = new EnderecoVO();
					Erro erro = new Erro();
					erro.setDescricao("Exclusao realizada com sucesso");
					erro.setCodigo(HttpStatus.OK.value());
					endereco.setErro(erro);
					return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.OK);
				} catch (Exception e) {
					endereco = new EnderecoVO();
					Erro erro = new Erro();
					erro.setDescricao("Nao foi possivel realizar exclusao");
					erro.setCodigo(HttpStatus.BAD_REQUEST.value());
					endereco.setErro(erro);
					return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.BAD_REQUEST);
				}
			}else{
				endereco = new EnderecoVO();
				Erro erro = new Erro();
				erro.setDescricao("Endereco Nao Encontrado, nao foi possivel exclusao");
				erro.setCodigo(HttpStatus.BAD_REQUEST.value());
				endereco.setErro(erro);
				return new ResponseEntity<EnderecoVO>((EnderecoVO) endereco, HttpStatus.BAD_REQUEST);	
			}
		}else{
			EnderecoVO endereco = new EnderecoVO();
			Erro erro = new Erro();
			erro.setDescricao("Dados Inválidos");
			erro.setCodigo(HttpStatus.BAD_REQUEST.value());
			endereco.setErro(erro);
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
}
