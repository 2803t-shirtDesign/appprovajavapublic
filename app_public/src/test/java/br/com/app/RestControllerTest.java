package br.com.app;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.google.gson.Gson;

import br.com.app.controllers.EnderecoController;
import br.com.app.service.EnderecoService;
import br.com.app.vo.EnderecoVO;

@RunWith(MockitoJUnitRunner.class)
public class RestControllerTest {

	@InjectMocks
	private EnderecoController enderecoController;

	private MockMvc mockMvc;
	
	@Spy
    private EnderecoService enderecoService;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(enderecoController).build();
	}

	@Test
	public void testGetbyCep() throws Exception {
		String cep = "08762120";

		mockMvc.perform(MockMvcRequestBuilders.get("/endereco/" + cep))
				.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void testAddEndereco() throws Exception {
		EnderecoVO enderecoVO = new EnderecoVO();
		enderecoVO.setBairro("Teste");
		enderecoVO.setCep("08762120");
		enderecoVO.setCidade("Teste");
		enderecoVO.setEstado("Teste");
		enderecoVO.setRua("Teste");
		enderecoVO.setComplemento("Teste");
		enderecoVO.setNumero("Teste");

		mockMvc.perform(MockMvcRequestBuilders.post("/add-endereco").contentType(MediaType.APPLICATION_JSON).content(new Gson().toJson(enderecoVO)))
				.andExpect(MockMvcResultMatchers.status().isOk());
		
	}
}
