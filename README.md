This README would normally document whatever steps are necessary to get your application up and running.

Tecnologia utilizada na criação do Projeto.
Java 8 Maven SpringBootApplication SprintRest

Com estas tecnologias foram possíveis criar os serviços que consomem e produzem retorno JSON.

**URLs dos serviços:**

GET - /enderecos - Listar os endereços cadastrados.

GET - /endereco/{cep} - Buscar um endereço atraves do CEP, caso endereço não seja encontrado na base de dados, utilizei um serviço do Correios que devolve os dados do CEP.

POST - /add-endereco - Cria um endereço.

PUT - /alter-endereco/{id} - Altera um endereço através do ID.

DELETE - /delete-endereco/{id} - Deleta um endereço.

**Classe EnderecoService. Lógica de busca de Endereço.** 
1 - Busca endereço endereço através do CEP na base de dados.
 
2 - Realiza um Loop caso endereço não seja encontrado adicionando o valor 0 a direita do CEP.

3 - Caso endereço não seja encontrado na base de dados o mesmo processo ocorre porem consultando dados de CEP dos correios adicionando o valor 0 a direita do CEP.

**Resposta questão 4**
Quando voce digita a URL de um site (http://www.netshoes.com.br) no browser e pressiona enter, explique da forma que preferir, o que ocorre nesse processo do protocolo HTTP entre o Client e o Server. (5 pontos)
Nossa internet funciona basicamente com a linha de Cliente e Servidor, quando realizamos uma chamada de alguma URL, seja via browser ou linha de comando, realizamos uma chamada para um server. Para entendermos melhor vamos citar as etapas para está chamada.

1 - Usuário utiliza um browser e informa na URL o site http://www.netshoes.com.br e pressiona enter. 

2 - Meu Browser denominado Cliente solicita informações para um Server de DNS onde está hospedado as informações da URL informada; 

3 - O DNS devolve para o Cliente um endereço informando onde está localizada as informações da URL solicitada. 

4 - O Cliente inicia uma tentativa de acessar as informações deste endereço. 

5 - O server correspondente a este endereço responde a solicitação de acesso as informações e devolve em forma de conteúdo para o Cliente(Browser)